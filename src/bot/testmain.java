package bot;

import java.awt.EventQueue;

import bot.fx.MainFrame;

class testmain {
	
	public static MainFrame test;
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				test = new MainFrame();
			}
		});
	}
}

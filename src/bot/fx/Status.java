package bot.fx;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

import org.powerbot.game.api.methods.tab.Skills;

import framework.components.BasePanel;
import framework.components.interfaces.IParentComponent;
import framework.util.BorderContainer;
import framework.util.Trace;

public class Status extends BasePanel {

	private static final long serialVersionUID = 1L;
	
	private final String CM_format = "Clay mined: ";
	private final String CS_format = "Clay softened: ";

	private BorderContainer time;
		private JLabel script_timer;

	private BorderContainer farmer;
		private JLabel clay_mined;
		private JLabel clay_clayd;

	private BorderContainer levels;
		private JProgressBar xp_till_level;
		
	private int current_level;

	public Status(IParentComponent parent) {
		super("Status", parent);
		
		current_level = 1;
	}
	
	private JLabel create_label() {
		JLabel l = new JLabel("uninitialized");
		l.setHorizontalAlignment(JLabel.CENTER);

		return l;
	}
	
	@Override
	public void graphics_init() {
		time = new BorderContainer("Time");
		script_timer = create_label();

		time.add(script_timer);
	
		farmer = new BorderContainer("Monetary gains");
			clay_mined = create_label();
			clay_clayd = create_label();
	
		farmer.add(clay_mined);
		farmer.add(clay_clayd);
	
		levels = new BorderContainer("Level");
			xp_till_level = new JProgressBar();
			xp_till_level.setStringPainted(true);
	
		levels.add(xp_till_level);

		this.add(time);
		this.add(farmer);
		this.add(levels);
	}
	
	private void updateTimers() {

		MainFrame M = (MainFrame)this.getRoot();
		script_timer.setText(M.script_timer.toElapsedString());
	}

	private void updateItems() {
		clay_mined.setText(CM_format + Integer.toString(nodes.AtMine.clay_mined));
		clay_clayd.setText(CS_format + 0);
	}

	private void updateEXP() {

		int required = Skills.XP_TABLE[current_level+1] - Skills.getExperience(Skills.MINING);
		
		xp_till_level.setValue( Skills.getExperience(Skills.MINING));
		xp_till_level.setString("+" + required + " [" + getNextLevelPercent(Skills.MINING) + "%]");
	}

	public void setExpBar() {

		Trace.out("exp: " + Skills.getExperience(Skills.MINING));
		Trace.out("level: " + Skills.getLevelAt(Skills.getExperience(Skills.MINING)));
		
		current_level = Skills.getLevelAt(Skills.getExperience(Skills.MINING));
		final int nextlevel_exp = Skills.XP_TABLE[current_level+1];

		_setExpBar(
			Skills.XP_TABLE[current_level],
			nextlevel_exp
		);
	}

	private int getNextLevelPercent(int index) {

		int nextlevel_exp = Skills.XP_TABLE[current_level+1];

		int exp_to_level = nextlevel_exp - Skills.getExperience(index);
		int exp_required = nextlevel_exp - Skills.XP_TABLE[current_level];
		
		return (exp_to_level * 100) / exp_required;
	}

	private void _setExpBar(final int min, final int max) {
		xp_till_level.setMinimum(min);
		xp_till_level.setMaximum(max);
	}

	@Override
	public void init() {
		setExpBar();
	}

	@Override
	public void update() {
		updateItems();
		updateEXP();
		updateTimers();
	}
}

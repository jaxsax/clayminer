package bot.fx;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import nodes.AtBank;

import org.powerbot.game.api.methods.Game;
import org.powerbot.game.api.methods.input.Mouse;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.wrappers.node.Item;
import org.powerbot.game.api.wrappers.widget.WidgetChild;

import framework.util.util;


public class GFX {

	public static void drawItemOverlay(Graphics2D g) {

		Item[] items = Inventory.getItems();
		for (Item i : items) {
			WidgetChild w = i.getWidgetChild();

			int id = i.getId();


			if (util.idInArray_bool(id, util.red_ids))
				g.setColor(Color.red);
			else if (util.idInArray_bool(id, util.green_ids))
				g.setColor(Color.green);
			else if (util.idInArray_bool(id, AtBank.pick_ids))
				g.setColor(Color.yellow);
			else
				g.setColor(Color.cyan);

			g.drawRect(w.getAbsoluteX(), w.getAbsoluteY(), w.getWidth(),
					w.getHeight());
		}
	}

	public static void drawMouseOverlay(Graphics arg0) {

		arg0.setColor(Color.orange);

		int mX = Mouse.getX();
		int mY = Mouse.getY();

		Dimension gameDimension = Game.getDimensions();
		int gdX = gameDimension.width;
		int gdY = gameDimension.height;

		/* Top left */
		arg0.drawLine(mX, mY, 0, 0);

		/* Top right */
		arg0.drawLine(mX, mY, gdX, 0);

		/* Bottom left */
		arg0.drawLine(mX, mY, 0, gdY);

		/* Bottom right */
		arg0.drawLine(mX, mY, gdY, gdX);
	}
}

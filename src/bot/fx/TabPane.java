package bot.fx;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import framework.components.BaseTabPane;
import framework.components.interfaces.IChildComponent;
import framework.components.interfaces.IParentComponent;

public class TabPane extends BaseTabPane {

	private static final long serialVersionUID = 1L;
	private List<IChildComponent> children;

	public TabPane(IParentComponent parent) {
		super(parent, "tab_pane");
		
		this.children = new ArrayList<IChildComponent>();
	}

	@Override
	public void graphics_init() {
		this.add(new ScriptSettings(this));
		this.add(new Status(this));
	}

	@Override
	public void init() {
		
		for (Component i : this.getComponents()) {
			if (i instanceof IChildComponent) {
				IChildComponent child = (IChildComponent) i;
				this.children.add(child);
			}
		}
		
		for (IChildComponent i : this.children) {
			i.init();
		}
	}

	@Override
	public void update() {
		
		for (IChildComponent i : this.children) {
			i.update();
		}
		
	}
}

package bot.fx;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;

import nodes.AtBank;
import nodes.AtMine;
import framework.components.BasePanel;
import framework.components.interfaces.IParentComponent;
import framework.util.BorderContainer;


public class ScriptSettings extends BasePanel {

	private static final long serialVersionUID = 1L;
	
	private BorderContainer bank_location_container;
	private JComboBox<String> bank_location;
		private JButton bl_set;

	private BorderContainer mine_location_container;
	private JComboBox<String> mine_location;
		private JButton ml_set;

	private JButton start;
	
	public ScriptSettings(IParentComponent parent) {
		super("Script Settings", parent);
	}

	@Override
	public void graphics_init() {

		
		bank_location_container = new BorderContainer("Banking");
		bank_location = new JComboBox<String>(AtBank.BL.getKeys());
		bl_set = new JButton("Set");
		bl_set.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AtBank.BL.setActiveArea("BankLocation", bank_location.getSelectedItem().toString());
			}
		});

		bank_location_container.add(bank_location);
		bank_location_container.add(bl_set);
	
		mine_location_container = new BorderContainer("Mining");
			mine_location = new JComboBox<String>(AtMine.ML.getKeys());
			ml_set = new JButton("Set");
			ml_set.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					//AtMine.ML.setActiveArea("MineLocation", mine_location.getSelectedItem().toString());
				}
			});

		mine_location_container.add(mine_location);
		mine_location_container.add(ml_set);
		
		start = new JButton("Start");
		start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				changeState(true);
			}
		});
		
		this.add(bank_location_container);
		this.add(mine_location_container);

		this.add(start);
	}

	@Override
	public void init() {
	}

	@Override
	public void update() {
	}
	
	private void changeState(final boolean b) {
		MainFrame root_frame = (MainFrame)this.getRoot();
		root_frame.script_handler.emitState(b);
	}
}

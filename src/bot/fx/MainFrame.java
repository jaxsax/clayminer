package bot.fx;

import java.awt.Dimension;

import org.powerbot.core.script.util.Timer;

import framework.components.BaseFrame;
import framework.events.impl.ScriptEmitter;

public class MainFrame extends BaseFrame {
	
	private static final long serialVersionUID = 1L;
	
	public ScriptEmitter script_handler;
	public Timer script_timer;
	
	public MainFrame() {
		super("Script Interface", new Dimension(400, 240));
		
		this.script_handler = new ScriptEmitter();
		this.script_timer = new Timer(0);
	}

	@Override
	public void graphics_init() {
		this.addChild(new TabPane(this));
	}
}

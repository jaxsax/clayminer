package bot;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.powerbot.core.script.ActiveScript;
import org.powerbot.core.script.job.LoopTask;
import org.powerbot.core.script.job.Task;
import org.powerbot.core.script.job.state.Node;
import org.powerbot.core.script.job.state.Tree;
import org.powerbot.core.script.util.Random;
import org.powerbot.game.api.Manifest;
import org.powerbot.game.api.methods.Game;
import org.powerbot.game.api.methods.interactive.Players;

import bot.fx.MainFrame;
import framework.events.impl.ScriptListener;
import framework.util.Condition;
import framework.util.util;

@Manifest(authors = { "frozen" }, name = "hi", description = "??", version = 0.1)
public class main extends ActiveScript implements ScriptListener {

	@SuppressWarnings("unused")
	private final List<Node> jobCollection = Collections
			.synchronizedList(new ArrayList<Node>());
	private Tree container = null;

	public MainFrame script_interface;
	
	@Override
	public void onStart() {

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				script_interface = new MainFrame();
			}
		});
		
		util.waitUntil(new Condition() {

			@Override
			public boolean active() {
				return script_interface != null;
			}

			@Override
			public void run() {
				
			}

			@Override
			public String message() {
				return "Waiting for interface to initialize";
			}
			
		}, 5000);
		
		script_interface.script_handler.addListener(this);
	}

	@Override
	public int loop() {
		
		if (container != null) {
			final Node j = container.state();
			if (j == null)
				return Random.nextInt(100, 150);

			container.set(j);
			getContainer().submit(j);
			j.join();
		}

		return Random.nextInt(100, 150);
	}

	private final class start_task extends Task {

		@Override
		public void execute() {
			util.waitUntil(new Condition() {

				@Override
				public boolean active() {
					return script_interface.script_handler.getLastState() &&
							Game.isLoggedIn() &&
							Players.getLocal().isOnScreen();
				}

				@Override
				public void run() {
					script_interface.init();
					getContainer().submit(new InterfaceLoop());
				}

				@Override
				public String message() {
					return "Waiting for game to ready up";
				}
				
			}, 5000);
		}
	}
	
	private final class InterfaceLoop extends LoopTask {

		@Override
		public int loop() {
			
			script_interface.update();
			return 0;
		}
		
	}

	@Override
	public void onScriptStart() {
		getContainer().submit(new start_task());
	}

	@Override
	public void onScriptStop() {
		
	}
}
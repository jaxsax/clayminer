package nodes;

import locations.BankLocation;

import org.powerbot.core.script.job.state.Node;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.methods.widget.Bank;

import framework.util.util;


public class AtBank extends Node {

	public static final int[] pick_ids = {
		1265, /* Bronze pickaxe */
		1267, /* Iron pickaxe */
		1269, /* Steel pickaxe */
		1271, /* Adamant pickaxe */
		1273, /* Mithril pickaxe */
		1275, /* Rune pickaxe */
	};

	public static final BankLocation BL = new BankLocation();

	@Override
	public boolean activate() {

		return 	BL.inside(Players.getLocal().getLocation()) &&
				!util.inventoryempty(pick_ids);
	}

	@Override
	public void execute() {
		util.DropExcept(pick_ids);

		if (Bank.isOpen() && util.inventoryempty(pick_ids))
			Bank.close();
	}
}

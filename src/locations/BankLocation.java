package locations;

import org.powerbot.game.api.wrappers.Tile;

import framework.util.Location;


public class BankLocation extends Location {

	public BankLocation() {

		add("Varrock West", new Tile[] {
			new Tile(3182, 3446, 0),
            new Tile(3189+1, 3435, 0)
		});
	}
}
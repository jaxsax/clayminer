package locations;

import org.powerbot.game.api.wrappers.Tile;

import framework.util.Location;


public class MineLocation extends Location {

	public MineLocation() {

		add("Varrock West", new Tile[] {
			new Tile(3178, 3371, 0),
            new Tile(3181, 3374, 0)
		});

		//setActiveArea(this.getClass().getSimpleName(), loc);
	}
}

package framework.components.interfaces;

import java.util.List;

import javax.swing.JComponent;

public interface IParentComponent extends IBaseComponent {
	
	public List<IChildComponent> getChildren();
	public void addChild(JComponent child);
}

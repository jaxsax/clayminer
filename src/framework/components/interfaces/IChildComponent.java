package framework.components.interfaces;

import java.awt.Component;

public interface IChildComponent extends IBaseComponent {
	
	public IParentComponent getOwner();
	public Component getRoot();
}

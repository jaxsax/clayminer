/**
 * Every [Graphical Component] must have these methods
 * 
 */

package framework.components.interfaces;

public interface IBaseComponent {

	public void graphics_init();
	public void init();
	public void update();
	
}

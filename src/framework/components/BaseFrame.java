package framework.components;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;

import framework.components.interfaces.IChildComponent;
import framework.components.interfaces.IParentComponent;
import framework.util.Trace;

public abstract class BaseFrame extends JFrame implements IParentComponent {

	private static final long serialVersionUID = 1L;
	
	private List<IChildComponent> children;
	public BaseFrame(final String title, final Dimension size) {
		
		this.setTitle(title);
		this.setPreferredSize(size);
		this.children = new ArrayList<IChildComponent>();
		
		this.graphics_init();
		
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void init() {
		
		Trace.out(this.getName() + " has " + this.children.size() + " children");
		for (IChildComponent child : this.children) {
			child.init();
		}
	}

	@Override
	public void update() {

		for (IChildComponent child : this.children) {
			child.update();
		}
	}

	@Override
	public List<IChildComponent> getChildren() {
		return this.children;
	}

	@Override
	public void addChild(JComponent child) {
		
		if (!(child instanceof IParentComponent) && !(child instanceof IChildComponent)) {
			Trace.out("unknown object, add component");
			this.add(child);
			return;
		}

		this.children.add((IChildComponent)child);
		this.add(child);
	}
}

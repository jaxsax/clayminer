package framework.components;

import java.awt.Component;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import framework.components.interfaces.IChildComponent;
import framework.components.interfaces.IParentComponent;

public abstract class BaseTabPane extends JTabbedPane implements IChildComponent, IParentComponent {
	

	private static final long serialVersionUID = 1L;
	
	private IParentComponent parent;
	
	public BaseTabPane(IParentComponent parent, final String name) {
		this.parent = parent;
		this.setName(name);
		
		this.graphics_init();
	}

	@Override
	public IParentComponent getOwner() {
		return this.parent;
	}
	
	@Override
	public Component getRoot() {
		return SwingUtilities.getRoot(this);
	}
	
	@Override
	public List<IChildComponent> getChildren() {
		return null;	
	}

	@Override
	public void addChild(JComponent child) {}
}

package framework.components;

import java.awt.Component;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import framework.components.interfaces.IChildComponent;
import framework.components.interfaces.IParentComponent;

public abstract class BasePanel extends JPanel implements IChildComponent, IParentComponent {
	
	private static final long serialVersionUID = 1L;
	
	private IParentComponent parent;
	
	public BasePanel(final String name, IParentComponent parent) {
		this.parent = parent;
		
		this.setName(name);
		this.graphics_init();
	}

	@Override
	public IParentComponent getOwner() {
		return this.parent;
	}
	
	@Override
	public List<IChildComponent> getChildren() {
		return null;	
	}

	@Override
	public void addChild(JComponent child) {}
	
	@Override
	public Component getRoot() {
		return SwingUtilities.getRoot(this);
	}
}

package framework.util;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class BorderContainer extends JPanel {

	private static final long serialVersionUID = 1L;

	private TitledBorder tb;

	public BorderContainer(final String title) {

		this.tb = BorderFactory.createTitledBorder(
			BorderFactory.createLineBorder(Color.black),
			title
		);

		this.setBorder(tb);
		this.setLayout(new GridLayout(0, 1));
	}
}

package framework.util;

public interface Condition {

	public boolean active();
	public void run();
	public String message();
}
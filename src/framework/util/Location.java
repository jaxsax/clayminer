package framework.util;

import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;

import org.powerbot.core.Bot;
import org.powerbot.game.api.methods.interactive.Players;
import org.powerbot.game.api.wrappers.Area;
import org.powerbot.game.api.wrappers.Tile;

/* TODO: improve this */
public abstract class Location {

	private Map<String, Tile[]> tile_map = new HashMap<String, Tile[]>();

	private String active_area_str = "";
	private Area active_area;
	public Tile[] area_tilemap;

	public void add(String name, Tile[] tiles) {

		if (tiles.length <= 1) {
			Trace.out("Location: [" + name + "] requires two points, skipping");
			return;
		}

		tile_map.put(name, tiles);
	}

	public void setActiveArea(final String caller, final String key) {
		if (!tile_map.containsKey(key)) {
			Trace.out("Unable to locate " + key);
			Bot.instance().stop(); /* will never hit though ?? */
		}

		if (active_area_str.equals(key)) return;

		active_area_str = key;
		Trace.out("Setting active [" + caller + "] to [" + key + "]");

		active_area = new Area(tile_map.get(key));
		area_tilemap = active_area.getTileArray();
	}

	public void drawOverlay(boolean condition, Graphics g) {
		if (condition)
			_drawOverlay(g);
	}

	private void _drawOverlay(Graphics arg0) {

		if (active_area == null)
			return;

		arg0.setColor(util.opaque_green);
		if (!inside(Players.getLocal().getLocation()))
			arg0.setColor(util.opaque_red);

		for (Tile t : area_tilemap) {
			if (!t.isOnScreen()) continue;

			t.draw(arg0);
		}
	}

	public boolean inside(Tile t) {
		return active_area.contains(t);
	}

	public String[] getKeys() {
		return tile_map.keySet().toArray(new String[tile_map.keySet().size()]);
	}
}

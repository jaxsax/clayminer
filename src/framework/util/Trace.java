package framework.util;
/* TODO: use something better */
public class Trace {

	private static String out = "";
	private static boolean log = true;

	private static String getCallerClass() {
		return Thread.currentThread().getStackTrace()[3].getClassName();
	}

	private static String getMethodName() {
		return Thread.currentThread().getStackTrace()[3].getMethodName();
	}

	public static void out(final Object object) {

		final String string = object != null ? object.toString() : "null";

		if (!log) return;
		if (string.equals(out)) return;
		out = string;

		System.out.printf("[%s -> %s] %s\n",
			getCallerClass(),
			getMethodName(),
			out
		);
	}
}
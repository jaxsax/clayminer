package framework.util;

import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.powerbot.core.script.job.Task;
import org.powerbot.core.script.util.Timer;
import org.powerbot.game.api.methods.tab.Inventory;
import org.powerbot.game.api.methods.widget.Bank;
import org.powerbot.game.api.wrappers.node.Item;

public class util {

	public static boolean waitFor(final Condition c, final long timeout) {
		final Timer t = new Timer(timeout);

		while (t.isRunning() && !c.active()) {

			Trace.out(c.message());
			Task.sleep(50);
		}

		if (c.active())
			c.run();

		return c.active();
	}
	
	public static boolean waitUntil(final Condition c, final long timeout) {
		
		final Timer t = new Timer(timeout);
		int counter = 0;
		
		while(!c.active()) {
			
			counter++;
			while(t.isRunning() && !c.active()) {
				
				Trace.out(c.message() + " [" + counter + "]");
				Task.sleep(50);
			}
			
			t.reset();
		}
		
		if (c.active())
			c.run();
		return c.active();
	}
	
	public static int idInArray_count(int v, int... arr) {

		int matches = 0;
		for(int i = 0; i < arr.length; i++)
			if (v == arr[i]) matches++;

		return matches;
	}

	public static boolean idInArray_bool(int v, int... arr) {

		return idInArray_count(v, arr) > 0 ? true : false;
	}


	public static boolean inventoryempty(int... arr) {
		int items_matching_arr = 0;

		for (Item it : Inventory.getItems())
			items_matching_arr += idInArray_count(it.getId(), arr);

		final int r = Inventory.getCount() - items_matching_arr;
		Trace.out(
			Inventory.getCount() + " - " + items_matching_arr + " = " +
			r + " status: " + (r >= 1)
		);

		if ((Inventory.getCount() - items_matching_arr) >= 1)
			return false;

		return true;
	}

	public static final void DropExcept(int... keep_ids) {

		Map<Integer, Integer> unique_item_count = new HashMap<Integer, Integer>();
		for (Item i :  Inventory.getItems()) {

			int id = i.getId();
			if (idInArray_bool(id, keep_ids)) continue;

			if (unique_item_count.containsKey(id))
				unique_item_count.put(id, unique_item_count.get(id) + 1);
			else
				unique_item_count.put(id, 1);
		}

		Iterator<?> it = unique_item_count.entrySet().iterator();

		Timer wait = new Timer(2000);

		if (!Bank.isOpen())
			Bank.open();

		while(wait.isRunning()) {

			if (it.hasNext()) {
				@SuppressWarnings("rawtypes")
				Map.Entry kv = (Map.Entry) it.next();
				Trace.out("[DEPOSIT] " + kv.getKey() + " x" + kv.getValue());
				Bank.deposit((int)kv.getKey(), (int)kv.getValue());

				it.remove();
			}

			if (!inventoryempty(keep_ids))
				wait.reset();

			Task.sleep(200, 400);
		}
	}

	public static final Color opaque_green = new Color(0, 255, 0, 60);
	public static final Color opaque_red = new Color(255, 0, 0, 60);
	public static final Color opaque_white = new Color(255, 255, 255, 60);

	public static final int[] green_ids = {
		1761, /* Soft clay */
	};

	public static final int[] red_ids = {
		434, /* Clay */
	};
}

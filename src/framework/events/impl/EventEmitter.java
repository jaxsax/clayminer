package framework.events.impl;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

public abstract class EventEmitter<T, E> {
	
	private List<T> listeners;
	private E last_event_recv;
	
	public EventEmitter() {
		listeners = new ArrayList<T>();
		last_event_recv = null;
	}
	
	public void addListener(T listener) {
		this.listeners.add(listener);
	}

	public List<T> getListeners() {
		return this.listeners;
	}
	
	public E getLastEvent() {
		return this.last_event_recv;
	}
	
	public void setLastEvent(E event) {
		this.last_event_recv = event;
	}
	
	public abstract void emit(E event);

}

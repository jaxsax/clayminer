package framework.events.impl;

public interface ScriptListener {
	
	public void onScriptStart();
	public void onScriptStop();
}

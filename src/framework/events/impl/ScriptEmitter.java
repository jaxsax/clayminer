package framework.events.impl;

import framework.events.ScriptEvent;

public class ScriptEmitter extends EventEmitter<ScriptListener, ScriptEvent> {

	@Override
	public void emit(ScriptEvent event) {
		
		this.setLastEvent(event);
		for (ScriptListener listener : this.getListeners()) {
			;
		}
	}
	
	public void emitState(final boolean state) {
		
		ScriptEvent e = new ScriptEvent(this, state);
		this.setLastEvent(e);
		for (ScriptListener listener : this.getListeners()) {
			if (state)
				listener.onScriptStart();
			else
				listener.onScriptStop();
		}
	}

	public boolean getLastState() {
		if (this.getLastEvent() == null)
			return false;
		return this.getLastEvent().getState();
	}
}

/**
 * Needs more think through
 */

package framework.events;

import java.util.EventObject;

public class ScriptEvent extends EventObject {

	private static final long serialVersionUID = 1L;
	
	private boolean script_state;
	
	public ScriptEvent(Object arg0, boolean script_state) {
		super(arg0);
		
		this.script_state = script_state;
	}
	
	public boolean getState() {
		return this.script_state;
	}
}

/* I STILL NEED THIS
package fx.panel.tabs;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

import org.powerbot.game.api.methods.tab.Skills;

import framework.fx.BorderContainer;
import framework.fx.Tab;
import framework.util.Trace;

public class StatusTab extends Tab {

	private static final long serialVersionUID = 1L;

	final String CM_format = "Clay mined: ";
	final String CS_format = "Clay softened: ";

	private BorderContainer time;
		private JLabel script_timer;

	private BorderContainer farmer;
		private JLabel clay_mined;
		private JLabel clay_clayd;

	private BorderContainer levels;
		private JProgressBar xp_till_level;

	private int current_level;

	public StatusTab() {
		super("Status");

		current_level = 0;
	}


	/*public void pre_init() {

		time = new BorderContainer("Time");
			script_timer = create_label();

		time.add(script_timer);

		farmer = new BorderContainer("Monetary gains");
			clay_mined = create_label();
			clay_clayd = create_label();

		farmer.add(clay_mined);
		farmer.add(clay_clayd);

		levels = new BorderContainer("Level");
			xp_till_level = new JProgressBar();
			xp_till_level.setStringPainted(true);

		levels.add(xp_till_level);

		_setExpBar(0, 0);

		this.add(time);
		this.add(farmer);
		this.add(levels);
} *

	private JLabel create_label() {
		JLabel l = new JLabel("uninitialized");
		l.setHorizontalAlignment(JLabel.CENTER);

		return l;
	}

	public void updateTimers() {
		//script_timer.setText(main.gui_frame.script_timer.toElapsedString());
	}

	public void updateItems() {
		clay_mined.setText(CM_format + Integer.toString(nodes.AtMine.clay_mined));
		clay_clayd.setText(CM_format + 0);
	}

	public void updateEXP() {

		int required = Skills.XP_TABLE[current_level+1] - Skills.getExperience(Skills.MINING);
		xp_till_level.setString("+" + required + " [" + getNextLevelPercent(Skills.MINING) + "%]");
	}

	public void setExpBar() {

		Trace.out("exp: " + Skills.getExperience(Skills.MINING));
		current_level = Skills.getLevelAt(Skills.getExperience(Skills.MINING));
		Trace.out("current level: " + current_level);
		final int nextlevel_exp = Skills.XP_TABLE[current_level+1];

		_setExpBar(
			Skills.XP_TABLE[current_level],
			nextlevel_exp
		);
	}

	private int getNextLevelPercent(int index) {

		int nextlevel_exp = Skills.XP_TABLE[current_level+1];

		int exp_to_level = nextlevel_exp - Skills.getExperience(index);
		int exp_required = nextlevel_exp - Skills.XP_TABLE[current_level];

		Trace.out("exp_to_level = " + nextlevel_exp + " - " + Skills.getExperience(index) + " = " + exp_to_level);
		Trace.out("exp_required = " + nextlevel_exp + " - " + Skills.XP_TABLE[current_level]);
		Trace.out("% = " + (exp_to_level * 100) / exp_required);

		return (exp_to_level * 100) / exp_required;
	}

	private void _setExpBar(final int min, final int max) {
		xp_till_level.setMinimum(min);
		xp_till_level.setMaximum(max);
	}

	@Override
	public void update() {
		updateEXP();
		updateTimers();
		updateItems();
	}

	@Override
	public void init() {

		setExpBar();
	}
}
*/
